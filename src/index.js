import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
//boostrap import
import 'bootstrap/dist/css/bootstrap.min.css'
// AppNavbar import
import AppNavbar from './components/AppNavbar';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />

  </React.StrictMode>
);


// const name = "John Smith";
// const user = {
//   firstname: 'Jane',
//   lastanme : 'Smith'
// }

// function formatName(user){
//   return user.firstname + " " + user.lastanme;
// }

// const element = <h1>Hello,  {formatName(user)}</h1>

// root.render(element);


