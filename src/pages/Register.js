
import {Form, Button, NavItem} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register() {
    //retrieve from app.js and local storage
    const {user, setUser} = useContext(UserContext);

    //for redirect to different URL
    const navigate = useNavigate();

    //state hooks -> store values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNumber, setMobileNumber] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    

    const [isActive, setIsActive] = useState(false);

    // console.log(email);
    // console.log(password1);
    // console.log(password2);

    useEffect((e) => {
        // validation to enable register button when all fields are
        //populated and both password match
        if(firstName !== '' && lastName !== '' &&
            email !== '' &&  mobileNumber.toString().length > 10 &&
            password1 !== '' &&  password2 !== '' && 
            (password1 === password2))
        {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    },[firstName, lastName, email, mobileNumber, password1, password2]);



      //prevent from refresh after clicking the submit button
      function registerUser(e){
        e.preventDefault();



        //    [POST METHOD - fetch data]
        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
              email: email
            })
        })
        .then(res => res.json())
        .then(data => {

          //testing/verification only
          console.log("email: " + data);

          //test if the data is T/F
          if(data === true) {

            //sweet alert if the data is true
            //meaning email has duplicate
            Swal.fire({
              title: "Duplicate email found!",
              icon: "error",
              text: "Cannot register. Email has duplicate"
            })

          //if the returned data is false
          //meaning the email has no duplicate
          } else {

            fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                method: 'POST',
                headers : {
                  'Content-Type' : 'application/json'
                },
                body : JSON.stringify({
                    firstName : firstName,
                    lastName : lastName,
                    email : email,
                    mobileNo : mobileNumber,
                    password : password1
                })
            })
            .then(res => res.json())
            .then(data => {
            
                //test only or verification
                console.log("register :" + data);

                //test the data if returned True / False 
                if(data === true) {

                  //fire the sweet alert if True
                  // meaning successful register
                  Swal.fire({
                    title: "Successfully registered!",
                    icon: "success",
                    text: `${firstName}, you're now a registered user!`
                  })

                  //after successful register
                  //redirect the user to login page
                  navigate('/login');


                //if the the register is unsuccessful
                // the returned data if false
                } else {

                    //trigger the sweet alert for unsuccessful register
                    Swal.fire({
                      title: "Something's wrong!",
                      icon: "error",
                      text: "Cannot proceed."
                    })
                }
            })  
          }
        })


        // //for reset after submit
        // setFirstName('');
        // setLastName('');
        // setEmail('');
        // setMobileNumber('');
        // setPassword1('');
        // setPassword2('');

      } //end of function registerUser(e)


    return (

      (user.id !== null) ?
        <Navigate to="/courses"></Navigate>
      :

        <>
          <Form onSubmit={(e) => registerUser(e)}>

            {/* FIRST NAME FIELD */}
            <Form.Group className="mb-3" controlId="userFirstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control type="text" placeholder="First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
            </Form.Group>

            {/* LAST NAME FIELD */}
            <Form.Group className="mb-3" controlId="userLastName">
            <Form.Label>Last Name</Form.Label>
            <Form.Control type="text" placeholder="Last Name"  value={lastName} onChange={e => setLastName(e.target.value)} required/>
            </Form.Group>


            {/* EMAIL FIELD */}
            <Form.Group className="mb-3" controlId="userEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control type="email" placeholder="Email Address" value={email} onChange={e => setEmail(e.target.value)} required/>
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>

            {/* MOBILE NUMBER FIELD */}
            <Form.Group className="mb-3" controlId="userMobileNumber">
            <Form.Label>Mobile Number</Form.Label>
            <Form.Control type="Number" placeholder="Mobile Number" value={mobileNumber} onChange={e => setMobileNumber(e.target.value)}  required/>
            </Form.Group>

            {/* PASSWORD FIELD */}
            <Form.Group className="mb-3" controlId="password1">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
            </Form.Group>

             {/* VERIFY PASSWORD FIELD */}
            <Form.Group className="mb-3" controlId="password2">
              <Form.Label>Verify Password</Form.Label>
              <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required />
            </Form.Group> 
          
              {/* conditional rendering 
                  if true - show active button
                  if false - show disabled button
              */}
            {
              (isActive) ?
              <Button variant="primary" type="submit" controlId="submitBtn">
                  Register
              </Button>
              :
              <Button variant="primary" type="submit" controlId="submitBtn" disabled>
                  Register
              </Button>
            }
          </Form> 
        </> 
        
    )

}