import {Row, Col, Nav, NavLink} from 'react-bootstrap'


export default function PageNotFound(){

    return (
        <Row className="pt-5">
            <Col>
            <h2>Error 404: Page Not Found!</h2>
            <h6>Go back to <a href="http://localhost:3000/">Homepage</a> </h6>
      
            </Col>
        </Row>

        
 
    )

}