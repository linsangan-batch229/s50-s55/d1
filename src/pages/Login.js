import { useEffect, useState, useContext } from 'react';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2'


export default function Login(){

    const {user, setUser} = useContext(UserContext);

    //hooks
    const [userEmail, setUserEmail] = useState('');
    const [userPassword, setUserPassword] = useState('');

    //for button hooks
    const [btnIsActive, setBtnIsActive] = useState(false);

    //useEffect
    useEffect((e) => {
        // Validation to enable submit button when all fields are populated and both passwords match
        if(userEmail !== '' && userPassword !== ''){
            setBtnIsActive(true);
        } else {
            setBtnIsActive(false);
        }
    }, [userEmail, userPassword]);


    //prevent from refresh after clicking the submit button
    function loginUser(e){
        e.preventDefault();

        //process a fetch request to the corresponding
        // Syntax:
        //      fetch('url', {options})
        //      .then(res => res.json())
        //      .then(data => {})

        fetch('http://localhost:4000/users/login', {
            method: 'POST',
            headers : {
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify({
                email : userEmail,
                password : userPassword
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

        // If no user information is found, the "access" property will not be available and will return undefined
        // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type

            if(typeof data.access !== "undefined"){
                localStorage.setItem('token', data.access)
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt"
                })

            } else {
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again!"
                })
            }



        })






        // //save sa local storage
        // //Syntax - localStorage.setItem('propertyName', value)
        // //saving something in our local storage
        // localStorage.setItem('userEmail', userEmail);

        // //getItem - retrieveing item from local
        // setUser({
        //     userEmail : localStorage.getItem('userEmail')
        // })


        //default
        setUserEmail('');
        setUserPassword('');
       
    }


    //          [GET DETAILS FROM TOKEN]

    const retrieveUserDetails  = (token) => {
        // The token will be sent as part of the request's header information
        // We put "Bearer" in front of the token to follow implementation standards for JWTs

        fetch('http://localhost:4000/users/details', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
            setUser({
                id: data._id,
                isAdmin : data.isAdmin
            })
        })

    } 




    
    return (

    (user.id !== null) ?

    <Navigate to="/courses"></Navigate>

    :
    <Form onSubmit={(e) => loginUser(e)}>
      <Form.Group className="mb-3" controlId="userLoginEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email"  value={userEmail}
            onChange={(e)=>setUserEmail(e.target.value)} required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="userLoginPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" value={userPassword}
            onChange={(e)=>setUserPassword(e.target.value)} required
        />
      </Form.Group>

        {
        (btnIsActive) ?
        <Button variant="success" type="submit" >
            Submit
        </Button>
            :
        <Button variant="success" type="submit" disabled>
            Submit
        </Button>
        }
        
    </Form>

    );
}