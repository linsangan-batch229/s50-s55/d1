import {useContext, useEffect} from 'react'
import { Navigate } from "react-router-dom";
import UserContext from '../UserContext';

export default function  Logout() {

    // //para mabura yung laman ni localbrowser
    // localStorage.clear();

    //Use the UserContext object and destructure it to access
    //the setUser and unsetUser from the context provideer
    const {unSetUser, setUser}  = useContext(UserContext);
    
    //clear the localStorage of the user's information
    unSetUser();


    useEffect(() => {
        //Set the user state back to its original value 
        setUser({id: null})
    })


    return(
        <Navigate to="/login"/>

    )

}