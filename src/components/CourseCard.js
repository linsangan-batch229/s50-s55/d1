import { useState, useEffect } from 'react';
import {Card, Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom'

export default function CourseCard({courseProps}){
//Destructuring thedata to avoid dot notation
const {name, description, price, _id} = courseProps;

    //3 hooks in React
    // 1. useState
    // 2. useEffect
    // 3. useContext


    // Use the useState hook for the component to be able to store state
    // States are used to keep track of information related to individual
    //components
    // Syntax -> cons [getter, setter] = useState(initialGetterValue)

    const [count, setCount] = useState(0);
    console.log(useState(0));

    const [seat, setSeat] = useState(30);


    // function enroll(){
    //     if(seat !== 0){
    //         setCount(count + 1);
    //         setSeat(seat - 1);
    //     } else {
    //         alert('No more seats available.');
    //     }
    // }

    return(
        <Card className='my-3'>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description</Card.Subtitle>  
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Text>Enrolles: {count}</Card.Text>
                <Link className='btn btn-primary' to={`/courseView/${_id}`}>Details</Link>
            </Card.Body>
        </Card>




    )
}